<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-assignable-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Assignable;

use Stringable;

/**
 * AssignableInterface interface file.
 * 
 * This interface is not meant to be implemented directly. This interface is
 * a superinterface from which other interfaces from other libraries that
 * need the assignable trait may use as superinterface.
 * 
 * @author Anastaszor
 */
interface AssignableInterface extends Stringable
{
	
	/**
	 * Gets whether this object is already assigned.
	 * 
	 * @return boolean whether this object is already assigned
	 */
	public function isAssigned() : bool;
	
	/**
	 * Gets the assignment key of this record, if any.
	 * 
	 * @return string
	 */
	public function getAssignmentId() : string;
	
	/**
	 * Assigns this record to the record with given identifier.
	 * 
	 * @param string $assignedId
	 * @return boolean whether the assignment is a success
	 */
	public function assign(string $assignedId) : bool;
	
}
